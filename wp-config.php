<?php
/**
 * Grundeinstellungen für WordPress
 *
 * Zu diesen Einstellungen gehören:
 *
 * * MySQL-Zugangsdaten,
 * * Tabellenpräfix,
 * * Sicherheitsschlüssel
 * * und ABSPATH.
 *
 * Mehr Informationen zur wp-config.php gibt es auf der
 * {@link https://codex.wordpress.org/Editing_wp-config.php wp-config.php editieren}
 * Seite im Codex. Die Zugangsdaten für die MySQL-Datenbank
 * bekommst du von deinem Webhoster.
 *
 * Diese Datei wird zur Erstellung der wp-config.php verwendet.
 * Du musst aber dafür nicht das Installationsskript verwenden.
 * Stattdessen kannst du auch diese Datei als wp-config.php mit
 * deinen Zugangsdaten für die Datenbank abspeichern.
 *
 * @package WordPress
 */

// ** MySQL-Einstellungen ** //
/**   Diese Zugangsdaten bekommst du von deinem Webhoster. **/

/**
 * Ersetze datenbankname_hier_einfuegen
 * mit dem Namen der Datenbank, die du verwenden möchtest.
 */
define('DB_NAME', 'martirho_wp1');

/**
 * Ersetze benutzername_hier_einfuegen
 * mit deinem MySQL-Datenbank-Benutzernamen.
 */
//define('DB_USER', 'root');
define('DB_USER', 'root');
/**
 * Ersetze passwort_hier_einfuegen mit deinem MySQL-Passwort.
 */
//define('DB_PASSWORD', '');
define('DB_PASSWORD', '');
/**
 * Ersetze localhost mit der MySQL-Serveradresse.
 */
define('DB_HOST', 'localhost');
//define('DB_HOST', 'localhost');

/**
 * Der Datenbankzeichensatz, der beim Erstellen der
 * Datenbanktabellen verwendet werden soll
 */
define('DB_CHARSET', 'utf8mb4');

/**
 * Der Collate-Type sollte nicht geändert werden.
 */
define('DB_COLLATE', '');

/**#@+
 * Sicherheitsschlüssel
 *
 * Ändere jeden untenstehenden Platzhaltertext in eine beliebige,
 * möglichst einmalig genutzte Zeichenkette.
 * Auf der Seite {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * kannst du dir alle Schlüssel generieren lassen.
 * Du kannst die Schlüssel jederzeit wieder ändern, alle angemeldeten
 * Benutzer müssen sich danach erneut anmelden.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';rb-wnf `.^wL1(H3C^Em^F$wIKw%FKc^ ^[gPBrnKBlsF$CUD/]M}n%T+1<x)%m');
define('SECURE_AUTH_KEY',  '^A#,:@[]_YmKgrc=)3LQRn e:%@_8)qimOE6}LHpMFM%}pNyH|&4D`MWN]?ng4.s');
define('LOGGED_IN_KEY',    '9pBq0e.Dp(Lft>trlUU=O-3z[x{SOiY?qTw#5.XBLUM{;wZ+<!m^NJ!/$6|ekw3f');
define('NONCE_KEY',        'Oh,C]ds6};dMwqV5|]:Bu/m<%*;pfC:}EU`pPJdN]OurUuH<YH#7!$XrhLnl=<`1');
define('AUTH_SALT',        'mF#,Uc<k-;hu>N/T>8^s@|D*.X#S30O9)4Dgi=w b1OQJ(i#VWADq2/O]13PzVx>');
define('SECURE_AUTH_SALT', '=u%aw+A| Fk9[w+J$J .`QQ<z25tkKchkRWQ2~7jJNE%R1{kL+vRT%g23J#:*=EK');
define('LOGGED_IN_SALT',   'zzizO&pa&L>X,=aLB`&g@QOzFgJ]>H5N&1fXSPskYE!/pB3GAFZG7C~WsM XTz2<');
define('NONCE_SALT',       'S2.kge}-gn~#U.$h= t-?m;|whdN}0ct%ka;0PU(V3s5W:G9TfBTE|X_H[S~@8[E');

/**#@-*/

/**
 * WordPress Datenbanktabellen-Präfix
 *
 * Wenn du verschiedene Präfixe benutzt, kannst du innerhalb einer Datenbank
 * verschiedene WordPress-Installationen betreiben.
 * Bitte verwende nur Zahlen, Buchstaben und Unterstriche!
 */
$table_prefix  = 'wp_';

/**
 * Für Entwickler: Der WordPress-Debug-Modus.
 *
 * Setze den Wert auf „true“, um bei der Entwicklung Warnungen und Fehler-Meldungen angezeigt zu bekommen.
 * Plugin- und Theme-Entwicklern wird nachdrücklich empfohlen, WP_DEBUG
 * in ihrer Entwicklungsumgebung zu verwenden.
 *
 * Besuche den Codex, um mehr Informationen über andere Konstanten zu finden,
 * die zum Debuggen genutzt werden können.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Das war’s, Schluss mit dem Bearbeiten! Viel Spaß beim Bloggen. */
/* That's all, stop editing! Happy blogging. */

/** Der absolute Pfad zum WordPress-Verzeichnis. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Definiert WordPress-Variablen und fügt Dateien ein.  */
require_once(ABSPATH . 'wp-settings.php');

set_time_limit(300);
